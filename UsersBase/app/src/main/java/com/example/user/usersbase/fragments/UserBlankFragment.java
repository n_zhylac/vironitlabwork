package com.example.user.usersbase.fragments;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.user.usersbase.App;
import com.example.user.usersbase.AppDatabase;
import com.example.user.usersbase.PieChartView;
import com.example.user.usersbase.R;
import com.example.user.usersbase.User;
import com.example.user.usersbase.UserDao;
import com.squareup.leakcanary.RefWatcher;

import java.io.File;
import java.util.List;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;


public class UserBlankFragment extends Fragment implements ChoosePhotoDialogFragment.DialogListenerInterface, SeekBar.OnSeekBarChangeListener {

    private static final String ARG_USER_ID = "user_id";
    private static final int REQUEST_GET_SINGLE_FILE = 1;
    private static final int REQUEST_GET_FILE_FROM_CAMERA = 2;

    private ImageView mAvatarImageButton;
    private EditText mNameEditText;
    private EditText mLastNameEditText;
    private EditText mTitleEditText;
    private ImageView mCallImageButton;
    private Button mAddUserButton;
    private EditText mPhoneNumberEditText;
    private TextView mUserProgressTextView;
    private PieChartView mPieChart;
    private ImageButton mDelImageButton;

    private User mUser;
    private AppDatabase db = App.getInstance().getDatabase();
    private UserDao mUserDao = db.userDao();
    private long mUserId;
    private Uri uri;

    private byte mDialogChoose;
    private File mCurrentPhoto;

    public static UserBlankFragment newInstance(long id) {

        Bundle args = new Bundle();
        args.putLong(ARG_USER_ID, id);

        UserBlankFragment fragment = new UserBlankFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserId = Objects.requireNonNull(getArguments()).getLong(ARG_USER_ID);

        if (mUserId == -1) {
            mUser = new User();
        } else {
            mUser = mUserDao.getById(mUserId);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_user_blank, container, false);
        mAvatarImageButton = v.findViewById(R.id.blank_avatar_image_button);
        mCallImageButton = v.findViewById(R.id.blank_call_image_button);
        mNameEditText = v.findViewById(R.id.blank_name_edit_text);
        mLastNameEditText = v.findViewById(R.id.blank_last_name_edit_text);
        mTitleEditText = v.findViewById(R.id.blank_title_edit_text);
        mPieChart = v.findViewById(R.id.pie_chart_view);
        mDelImageButton = v.findViewById(R.id.del_image_button);

        if (mUserId != -1) animation();

        if (mUserId == -1) {
            mAddUserButton = v.findViewById(R.id.blank_add_button);
            mPhoneNumberEditText = v.findViewById(R.id.blank_phone_number_edit_text);

            final SeekBar seekBar = (SeekBar) v.findViewById(R.id.seekBar);
            seekBar.setOnSeekBarChangeListener(this);
            mUserProgressTextView = (TextView) v.findViewById(R.id.progress_text_view);
            setToVisible(seekBar);

            mAddUserButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    validateControl(seekBar, v);
                }
            });
        } else {
            updateUI();
        }

        mCallImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mUser.mPhoneNumber));
                if (intent.resolveActivity(v.getContext().getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });

        mAvatarImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openWeightPicker();
            }
        });

        mDelImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUser.mAvatarUri.contains("usersbase") && !mUser.mAvatarUri.contains("drawable")) {
                    deleteLocalPhotoFile(mUser.mAvatarUri);
                    Toast.makeText(getContext(), "File deleted", Toast.LENGTH_SHORT).show();
                }
                mUserDao.delete(mUser);
                Objects.requireNonNull(getActivity()).onBackPressed();
            }
        });

        mPieChart.setProgressValue(mUser.mUserProgress);
        mPieChart.invalidate();


/* ТЕСТОВЫЙ ВЫЗОВ ОШИБКИ
        mPieChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    throw new RuntimeException("This is a crash");
            }
        });
*/
        return v;
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RefWatcher refWatcher = App.getRefWatcher(getActivity());
        refWatcher.watch(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_GET_SINGLE_FILE && data != null) {
            uri = data.getData();

            setAvatar(uri);

            if (mUser.mAvatarUri != null)
                if (mUser.mAvatarUri.contains("usersbase") && !mUser.mAvatarUri.contains("drawable")) {
                    deleteLocalPhotoFile(mUser.mAvatarUri);
                    Toast.makeText(getContext(), "File deleted", Toast.LENGTH_SHORT).show();
                }
            mUser.mAvatarUri = uri.toString();
            if (mUser.mUserName != null) {
                mUserDao.insert(mUser);
            }
            uri = null;
        }

        if (resultCode == RESULT_OK && requestCode == REQUEST_GET_FILE_FROM_CAMERA) {

            setAvatar(uri);

            if (mUser.mAvatarUri != null)
                if (mUser.mAvatarUri.contains("usersbase") && !mUser.mAvatarUri.contains("drawable")) {
                    deleteLocalPhotoFile(mUser.mAvatarUri);
                    Toast.makeText(getContext(), "File deleted", Toast.LENGTH_SHORT).show();
                }
            mUser.mAvatarUri = uri.toString();
            if (mUser.mUserName != null) {
                mUserDao.insert(mUser);
            }
            uri = null;
        }
    }

    private void setAvatar(Uri uri) {
        SharedPreferences sp = Objects.requireNonNull(getActivity()).getSharedPreferences(getString(R.string.settings_file_name), Context.MODE_PRIVATE);
        int i = sp.getInt(getActivity().getString(R.string.settings), 1);

        if (i == 1) {
            Glide
                    .with(getActivity())
                    .load(uri)
                    .apply(RequestOptions.circleCropTransform())
                    .into(mAvatarImageButton);
        } else {
            Glide
                    .with(getActivity())
                    .load(uri)
                    .apply(RequestOptions.noTransformation())
                    .into(mAvatarImageButton);
        }
    }

    public void updateUI() {
        mNameEditText.setText(mUser.mUserName);
        mLastNameEditText.setText(mUser.mUserLastName);
        mTitleEditText.setText(mUser.mUserTitle);

        setAvatar(Uri.parse(mUser.mAvatarUri));

        Glide
                .with(Objects.requireNonNull(getActivity()))
                .load(R.drawable.ic_phone_black_24dp)
                .apply(RequestOptions.circleCropTransform())
                .into(mCallImageButton);
    }

    public void openWeightPicker() {
        BottomSheetDialogFragment addPhotoBottomDialogFragment =
                ChoosePhotoDialogFragment.newInstance();
        ((ChoosePhotoDialogFragment) addPhotoBottomDialogFragment).registerCallBack(this);
        addPhotoBottomDialogFragment.show(Objects.requireNonNull(getFragmentManager()),
                "fragment_choose_photo_dialog");
    }

    @Override
    public void onChooseResult(byte i) {
        mDialogChoose = i;
        loadPhoto(mDialogChoose);
    }

    private void loadPhoto(byte dialogChoose) {
        switch ((int) dialogChoose) {
            case 1:
                mCurrentPhoto = new File(Objects.requireNonNull(getActivity()).getFilesDir(), android.text.format.DateFormat.
                        format("yyyy-MM-dd hh:mm:ss a", new java.util.Date()).toString() + ".jpg");
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                Uri storageUri = FileProvider.getUriForFile(getActivity(),
                        "com.example.user.usersbase.fileprovider", mCurrentPhoto);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, storageUri);

                List<ResolveInfo> cameraActivities = getActivity().
                        getPackageManager().queryIntentActivities(cameraIntent, PackageManager.MATCH_DEFAULT_ONLY);

                for (ResolveInfo activity : cameraActivities) {
                    getActivity().grantUriPermission(activity.activityInfo.packageName,
                            storageUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                }
                uri = storageUri;

                startActivityForResult(cameraIntent, REQUEST_GET_FILE_FROM_CAMERA);
                break;
            case 2:
                Intent galaryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galaryIntent.addCategory(Intent.CATEGORY_OPENABLE);
                galaryIntent.setType("image/*");
                startActivityForResult(Intent.createChooser(galaryIntent, "Select Picture"), REQUEST_GET_SINGLE_FILE);

                break;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        mUserProgressTextView.setText("User's Progress: " + String.valueOf(seekBar.getProgress()) + "%");
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    private void setToVisible(@NonNull SeekBar seekBar) {
        mAddUserButton.setVisibility(View.VISIBLE);
        mPhoneNumberEditText.setVisibility(View.VISIBLE);
        mCallImageButton.setVisibility(View.INVISIBLE);
        mUserProgressTextView.setVisibility(View.VISIBLE);
        seekBar.setVisibility(View.VISIBLE);
        mPieChart.setVisibility(View.INVISIBLE);
        mDelImageButton.setVisibility(View.INVISIBLE);
    }

    private void setToInvisible(@NonNull SeekBar seekBar) {
        mAddUserButton.setVisibility(View.INVISIBLE);
        mPhoneNumberEditText.setVisibility(View.INVISIBLE);
        mCallImageButton.setVisibility(View.VISIBLE);
        mUserProgressTextView.setVisibility(View.INVISIBLE);
        seekBar.setVisibility(View.INVISIBLE);
        mPieChart.setVisibility(View.VISIBLE);
        mDelImageButton.setVisibility(View.VISIBLE);
    }

    private void setUserFields(@NonNull SeekBar seekBar) {
        mUser.mUserName = mNameEditText.getText().toString();
        mUser.mUserLastName = mLastNameEditText.getText().toString();
        mUser.mUserTitle = mTitleEditText.getText().toString();
        mUser.mPhoneNumber = mPhoneNumberEditText.getText().toString();
        mUser.mUserProgress = seekBar.getProgress();
    }

    private void deleteLocalPhotoFile(String uriString) {
        File file = new File(Uri.parse(uriString).getPath());
        file.delete();
    }

    private Uri getSyastemPhotoUri() {
        return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
                getContext().getResources().getResourcePackageName(R.drawable.ic_photo_camera_24dp) + '/' +
                getContext().getResources().getResourceTypeName(R.drawable.ic_photo_camera_24dp) + '/' +
                getContext().getResources().getResourceEntryName(R.drawable.ic_photo_camera_24dp));
    }

    private void validateControl(SeekBar seekBar, View v) {

        if (mNameEditText.getText().toString().trim().length() == 0) {
            mNameEditText.setError("Name was not entered");
            mNameEditText.requestFocus();
            return;
        }

        if (mLastNameEditText.getText().toString().trim().length() == 0) {
            mLastNameEditText.setError("Last Name was not entered");
            mLastNameEditText.requestFocus();
            return;
        }

        if (mTitleEditText.getText().toString().trim().length() == 0) {
            mTitleEditText.setError("Title was not entered");
            mTitleEditText.requestFocus();
            return;
        }

        if (mPhoneNumberEditText.getText().toString().trim().length() == 0) {
            mPhoneNumberEditText.setError("Phone number was not entered");
            mPhoneNumberEditText.requestFocus();
            return;
        }

        if (mUser.mAvatarUri != null) {
            setUserFields(seekBar);
            mUser.id = mUserDao.insert(mUser);
            mPieChart.setProgressValue(mUser.mUserProgress);
            mPieChart.invalidate();
            setToInvisible(seekBar);
        } else {
            Toast toast = Toast.makeText(v.getContext(),
                    "Choose photo! \nOr press Add button again", Toast.LENGTH_SHORT);
            toast.show();
            mUser.mAvatarUri = getSyastemPhotoUri().toString();
        }

    }

    private void animation() {
        ObjectAnimator animationCallButton = ObjectAnimator.ofFloat(mCallImageButton, View.ALPHA, 0, 1);
        animationCallButton.setInterpolator(new AccelerateInterpolator());
        ObjectAnimator animationName = ObjectAnimator.ofFloat(mNameEditText, View.TRANSLATION_X, -1000, 0);
        animationName.setInterpolator(new AccelerateInterpolator());
        ObjectAnimator animationLastName = ObjectAnimator.ofFloat(mLastNameEditText, View.TRANSLATION_X, -1000, 0);
        animationLastName.setInterpolator(new AccelerateInterpolator());
        ObjectAnimator animationTitle = ObjectAnimator.ofFloat(mTitleEditText, View.TRANSLATION_X, -1000, 0);
        animationTitle.setInterpolator(new AccelerateInterpolator());
        ObjectAnimator animationPieChart = ObjectAnimator.ofFloat(mPieChart, View.ALPHA, 0, 1);
        animationPieChart.setInterpolator(new AccelerateInterpolator());
        ObjectAnimator animationAvatar = ObjectAnimator.ofFloat(mAvatarImageButton, View.ALPHA, 0, 1);
        animationAvatar.setInterpolator(new AccelerateInterpolator());
        ObjectAnimator animationDelButton = ObjectAnimator.ofFloat(mDelImageButton, View.ALPHA, 0, 1);
        animationAvatar.setInterpolator(new AccelerateInterpolator());

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(animationAvatar).with(animationName);
        animatorSet.play(animationAvatar).with(animationPieChart);
        animatorSet.play(animationAvatar).with(animationCallButton);
        animatorSet.play(animationAvatar).with(animationLastName);
        animatorSet.play(animationAvatar).with(animationTitle);
        animatorSet.play(animationAvatar).with(animationDelButton);
        animatorSet.setDuration(500);
        animatorSet.start();
    }
}
