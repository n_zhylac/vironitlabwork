package com.example.user.usersbase;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class User {

    @PrimaryKey(autoGenerate = true)
    public long id;
    public String mUserName;
    public String mUserLastName;
    public String mUserTitle;
    public String mAvatarUri;
    public String mPhoneNumber;
    public int mUserProgress;
}
