package com.example.user.usersbase.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.user.usersbase.App;
import com.example.user.usersbase.R;
import com.squareup.leakcanary.RefWatcher;

public class ChoosePhotoDialogFragment extends BottomSheetDialogFragment {

    private ImageButton mCameraImageButton;
    private ImageButton mGalaryImageButton;

    public interface DialogListenerInterface {
        void onChooseResult(byte i);
    }

    private DialogListenerInterface mListenerInterface;

    public static ChoosePhotoDialogFragment newInstance() {
        ChoosePhotoDialogFragment dialogFragment = new ChoosePhotoDialogFragment();
        return dialogFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_choose_photo_dialog, container,
                false);
        setCancelable(true);


        mCameraImageButton = (ImageButton) view.findViewById(R.id.dialog_camera_image_view);
        mCameraImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                mListenerInterface.onChooseResult((byte) 1);
            }
        });

        mGalaryImageButton = (ImageButton) view.findViewById(R.id.dialog_galary_image_view);
        mGalaryImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                mListenerInterface.onChooseResult((byte) 2);
            }
        });

        return view;

    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RefWatcher refWatcher = App.getRefWatcher(getActivity());
        refWatcher.watch(this);
    }

    public void registerCallBack(DialogListenerInterface listenerInterface) {
        mListenerInterface = listenerInterface;
    }
}
