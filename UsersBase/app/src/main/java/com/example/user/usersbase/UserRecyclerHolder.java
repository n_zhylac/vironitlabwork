package com.example.user.usersbase;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserRecyclerHolder extends RecyclerView.ViewHolder {

    @BindView
   (R.id.textView) TextView mNameTextView;
    @BindView
   (R.id.textView2) TextView mLastnameTextView;
    @BindView
   (R.id.textView3) TextView mTitleTextView;
    @BindView
   (R.id.imageView) ImageView mAvatarImageView;
    @BindView
   (R.id.imageButton) ImageButton mCallImageButton;
    @BindView
   (R.id.progressBar) ProgressBar mProgressBar;

    private String mUserPhoneNumber;
    private long usId;

    public UserRecyclerHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bind(User user, final UserRecyclerAdapter.ItemClickListener listener, Context context) {

        mNameTextView.setText(user.mUserName);
        mLastnameTextView.setText(user.mUserLastName);
        mTitleTextView.setText(user.mUserTitle);
        mUserPhoneNumber = user.mPhoneNumber;
        mProgressBar.setProgress(user.mUserProgress);
        usId = user.id;

        mCallImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onCallButtonClickListener(mUserPhoneNumber);
            }
        });

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onUserClickListener(usId);
            }
        });

        SharedPreferences sp = context.getApplicationContext().getSharedPreferences(context.getString(R.string.settings_file_name), Context.MODE_PRIVATE);
        int i = sp.getInt(context.getString(R.string.settings), 1);

        if (i == 1) {
            Glide
                    .with(mAvatarImageView.getContext())
                    .load(Uri.parse(user.mAvatarUri))
                    .apply(RequestOptions.circleCropTransform())
                    .into(mAvatarImageView);

        } else {
            Glide
                    .with(mAvatarImageView.getContext())
                    .load(Uri.parse(user.mAvatarUri))
                    .into(mAvatarImageView);
        }

        Glide
                .with(mCallImageButton.getContext())
                .load(R.drawable.ic_phone_black_24dp)
                .apply(RequestOptions.circleCropTransform())
                .into(mCallImageButton);
    }
}
