package com.example.user.usersbase.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.user.usersbase.App;
import com.example.user.usersbase.AppDatabase;
import com.example.user.usersbase.R;
import com.example.user.usersbase.User;
import com.example.user.usersbase.UserDao;
import com.example.user.usersbase.WiFiReceiver;
import com.example.user.usersbase.fragments.SettingsFragment;
import com.example.user.usersbase.fragments.UserListFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ListActivity extends LaunchActivity implements WiFiReceiver.WiFiCallBack {

    private String filename = "ReportFile.txt";
    private String jsonFilename = "JsonReportFile.json";
    private String filepath = "MyFileStorage";

    File myExternalFile;
    File myExternalJsonFile;

    private WiFiReceiver mWiFiReceiver;

    @Override
    protected Fragment createFragment() {
        Fragment fragment = new UserListFragment();
        return fragment;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mWiFiReceiver = new WiFiReceiver();
        mWiFiReceiver.registerWiFiCallback(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                Fragment fragment = SettingsFragment.newInstance();
                FragmentManager fm = getSupportFragmentManager();
                fm.beginTransaction()
                        .setCustomAnimations(R.anim.fragment_start, R.anim.fragment_stop)
                        .replace(R.id.fragment_container, fragment)
                        .addToBackStack(null)
                        .commit();
                return true;

            case R.id.send_file:
                AppDatabase db = App.getInstance().getDatabase();
                UserDao mUserDao = db.userDao();
                List<User> mUserList;
                mUserList = mUserDao.getAll();
                int size = mUserList.size();
                String buf;
                User user;

                if (size > 0) {

                    if (!isExternalStorageAvailable() || isExternalStorageReadOnly()) {
                    } else {
                        myExternalFile = new File(getExternalFilesDir(filepath), filename);
                        myExternalJsonFile = new File(getExternalFilesDir(filepath), jsonFilename);
                        Toast.makeText(this, myExternalJsonFile.getPath(), Toast.LENGTH_LONG).show();

                    }

                    try {
                        FileOutputStream fos = new FileOutputStream(myExternalFile);
                        do {
                            user = mUserList.get(size - 1);
                            buf = "Name: " + user.mUserName + "; Last Name: " + user.mUserLastName
                                    + "; Title: " + user.mUserTitle + "; Progress = "
                                    + user.mUserProgress + "% \n";
                            fos.write(buf.getBytes());
                        } while (--size > 0);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    try {
                        size = mUserList.size();
                        FileOutputStream fos = new FileOutputStream(myExternalJsonFile);
                        JSONArray jsonObject = new JSONArray();
                        do {
                            user = mUserList.get(size - 1);
                            jsonObject.put(toJson(user));
                        } while (--size > 0);
                        fos.write(jsonObject.toString().getBytes());
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    sendMail(myExternalFile, myExternalJsonFile);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    private static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    private void sendMail(File file, File jsonFile) {
        ArrayList<Uri> sendingFiles = new ArrayList<>();
        sendingFiles.add(Uri.fromFile(file));
        sendingFiles.add(Uri.fromFile(jsonFile));

        Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_STREAM, sendingFiles);
        startActivity(Intent.createChooser(intent, "SHARE RESULT"));
    }

    private JSONObject toJson(User user) {
        JSONObject object = new JSONObject();
        try {
            object.put("name", user.mUserName);
            object.put("lastname", user.mUserLastName);
            object.put("title", user.mUserTitle);
            object.put("phone", user.mPhoneNumber);
            object.put("progress", user.mUserProgress);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }

    @SuppressLint("ResourceType")
    @Override
    public void wifiCallback(String state) {
        Toast.makeText(this, state, Toast.LENGTH_LONG).show();
        Snackbar.make(findViewById(R.id.fragment_container), state, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }
}
