package com.example.user.usersbase;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;

public class WiFiReceiver extends BroadcastReceiver {

    public interface WiFiCallBack {
        void wifiCallback(String state);
    }

    private static WiFiCallBack mWiFiCallBack;

    @Override
    public void onReceive(Context context, Intent intent) {

        WifiManager manager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        switch (manager.getWifiState()) {
            case WifiManager.WIFI_STATE_ENABLING:
                mWiFiCallBack.wifiCallback("WIFI_STATE_ENABLE");
                return;
            case WifiManager.WIFI_STATE_DISABLING:
                mWiFiCallBack.wifiCallback("WIFI_STATE_DISABLE");
                return;
        }


    }

    public void registerWiFiCallback(WiFiCallBack callBack) {
        mWiFiCallBack = callBack;
    }
}
