package com.example.user.usersbase;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class PieChartView extends View {
    private Paint paint;
    private int mProgressValue;

    public PieChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
        paint = new Paint();
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float to;
        float from;

        to = mProgressValue * 360f / 100f;
        from = 360 - to;

        paint.setColor(Color.GREEN);
        canvas.drawArc(0, 0, getWidth(), getHeight(), 270, to, true, paint);

        paint.setColor(Color.GRAY);
        canvas.drawArc(0, 0, getWidth(), getHeight(), 270 + to, from, true, paint);

    }

    public void setProgressValue(int progressValue) {
        mProgressValue = progressValue;
    }
}
