package com.example.user.usersbase.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;

import com.example.user.usersbase.App;
import com.example.user.usersbase.R;
import com.squareup.leakcanary.RefWatcher;

import java.util.Objects;

import static android.content.SharedPreferences.Editor;

public class SettingsFragment extends Fragment {

    private RadioGroup mRadioGroup;
    private Button mOkButton;
    private SharedPreferences sp;
    private final int SETTINGS1 = 1;
    private final int SETTINGS2 = 2;


    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RefWatcher refWatcher = App.getRefWatcher(getActivity());
        refWatcher.watch(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_settings, container, false);
        mRadioGroup = (RadioGroup) v.findViewById(R.id.settings_radio_group);

        sp = Objects.requireNonNull(getActivity()).getSharedPreferences(getString(R.string.settings_file_name), Context.MODE_PRIVATE);
        final int currentValue = sp.getInt(getString(R.string.settings), 1);

        final int checkedRadioButtonId = ((currentValue == 1) ? R.id.circle_radio_button : R.id.rect_radio_button);
        mRadioGroup.check(checkedRadioButtonId);

        mOkButton = v.findViewById(R.id.ok_settings_button);
        mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int isValueChecked = mRadioGroup.getCheckedRadioButtonId();

                if (isValueChecked != checkedRadioButtonId) {
                    Editor ed = sp.edit();
                    ed.putInt(getString(R.string.settings), isValueChecked == R.id.circle_radio_button ? SETTINGS1 : SETTINGS2).apply();
                }
                getActivity().onBackPressed();
            }
        });

        return v;
    }
}
