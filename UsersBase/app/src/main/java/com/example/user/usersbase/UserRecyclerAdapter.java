package com.example.user.usersbase;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public class UserRecyclerAdapter extends RecyclerView.Adapter<UserRecyclerHolder> {

    private List<User> mUsers;
    private Context mContext;

    public interface ItemClickListener {
        void onUserClickListener(long usId);

        void onCallButtonClickListener(String phoneNumber);
    }

    private ItemClickListener mItemClickListener;

    public UserRecyclerAdapter(List<User> userList, Context context) {
        mUsers = userList;
        mContext = context;
    }

    @Override
    public UserRecyclerHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View rootView = inflater.inflate(R.layout.item_view, viewGroup, false);
        return new UserRecyclerHolder(rootView);
    }

    @Override
    public void onBindViewHolder(UserRecyclerHolder viewHolder, int i) {
        viewHolder.bind(mUsers.get(i), mItemClickListener, mContext);
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    public void registerCallBack(ItemClickListener listenerInterface) {
        mItemClickListener = listenerInterface;
    }
}
