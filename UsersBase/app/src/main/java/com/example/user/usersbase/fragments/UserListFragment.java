package com.example.user.usersbase.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.user.usersbase.App;
import com.example.user.usersbase.AppDatabase;
import com.example.user.usersbase.R;
import com.example.user.usersbase.User;
import com.example.user.usersbase.UserDao;
import com.example.user.usersbase.UserRecyclerAdapter;
import com.squareup.leakcanary.RefWatcher;

import java.util.List;
import java.util.Objects;

public class UserListFragment extends Fragment implements UserRecyclerAdapter.ItemClickListener {

    private RecyclerView mRecyclerView;
    private ImageButton mAddImageButton;
    private UserRecyclerAdapter mUserRecyclerAdapter;

    private AppDatabase db = App.getInstance().getDatabase();
    private UserDao mUserDao = db.userDao();
    private List<User> mUserList;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_list, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mUserList = mUserDao.getAll();

        mUserRecyclerAdapter = new UserRecyclerAdapter(mUserList, getActivity());
        mUserRecyclerAdapter.registerCallBack(this);
        mRecyclerView.setAdapter(mUserRecyclerAdapter);

        mAddImageButton = (ImageButton) view.findViewById(R.id.add_user_button);
        mAddImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                Fragment fragment = UserBlankFragment.newInstance(-1);

                fm.beginTransaction()
                        .setCustomAnimations(R.anim.fragment_start, R.anim.fragment_stop)
                        .replace(R.id.fragment_container, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RefWatcher refWatcher = App.getRefWatcher(getActivity());
        refWatcher.watch(this);
    }

    @Override
    public void onUserClickListener(long usId) {
        FragmentManager fm = ((AppCompatActivity) Objects.requireNonNull(getContext())).getSupportFragmentManager();
        Fragment fragment = UserBlankFragment.newInstance(usId);

        fm.beginTransaction()
                .setCustomAnimations(R.anim.fragment_start, R.anim.fragment_stop)
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCallButtonClickListener(String mUserPhoneNumber) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mUserPhoneNumber));
        if (intent.resolveActivity(Objects.requireNonNull(getContext()).getPackageManager()) != null) {
            startActivity(intent, null);
        }
    }
}
