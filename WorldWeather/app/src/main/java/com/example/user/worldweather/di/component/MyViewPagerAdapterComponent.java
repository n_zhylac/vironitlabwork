package com.example.user.worldweather.di.component;

import com.example.user.worldweather.Adapters.MyViewPagerAdapter;
import com.example.user.worldweather.di.module.MyViewPagerModule;
import com.example.user.worldweather.di.scopes.ApplicationScope;

import dagger.Component;

@ApplicationScope
@Component(modules = MyViewPagerModule.class)
public interface MyViewPagerAdapterComponent {
    MyViewPagerAdapter getPagerViewAdapter();
}
