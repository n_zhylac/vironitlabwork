package com.example.user.worldweather.di.component;

import com.example.user.worldweather.di.module.CityWeatherModule;
import com.example.user.worldweather.di.scopes.ApplicationScope;
import com.example.user.worldweather.retrofit.JsonPlaceHolderApi;

import dagger.Component;

@ApplicationScope
@Component(modules = CityWeatherModule.class)
public interface NetWorkServiceComponent {
    JsonPlaceHolderApi getCityWeatherApi();
}
