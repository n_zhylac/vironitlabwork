package com.example.user.worldweather.Adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.user.worldweather.ui.MapFragment;
import com.example.user.worldweather.ui.WeatherFragment;

public class MyViewPagerAdapter extends FragmentPagerAdapter {

    static final int PAGE_COUNT = 2;

    public static final MyViewPagerAdapter getInstance(FragmentManager fm){
        return new MyViewPagerAdapter(fm);
    }

    public MyViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i){
            case 0: return WeatherFragment.newInstance();
            case 1: return MapFragment.getInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0: return "Weather";
            case 1: return "Map";
        }
        return null;
    }
}
