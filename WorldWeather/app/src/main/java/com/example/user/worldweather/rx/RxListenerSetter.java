package com.example.user.worldweather.rx;

import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.EditText;

import io.reactivex.Observable;

public class RxListenerSetter {
    public static Observable<String> getViewClickerObservable(@NonNull final Button button) {

        return Observable.create(e -> {
            e.setCancellable(() -> button.setOnClickListener(null));
            button.setOnClickListener(v -> e.onNext(button.getText().toString()));
        });
    }

    public static Observable<String> getViewClickerObservable(@NonNull final EditText editText) {
        return Observable.create(e -> {
            e.setCancellable(() -> editText.setOnClickListener(null));
            editText.setOnClickListener(v -> e.onNext(editText.getText().toString()));
        });
    }

    public static Observable<Integer> getKeyListenerObservable(@NonNull final EditText editText) {
        return Observable.create(e -> {
            e.setCancellable(() -> editText.setOnKeyListener(null));
            editText.setOnKeyListener((view, i, keyEvent) -> {
                if (i == KeyEvent.KEYCODE_ENTER) {
                    e.onNext(i);
                    return true;
                }
                return false;
            });
        });
    }

    public static Observable<Integer> getPageChangeListener(@NonNull final ViewPager pager){
        return Observable.create(e -> {
            e.setCancellable(() -> pager.addOnPageChangeListener(null));
            pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i1) {

                }

                @Override
                public void onPageSelected(int i) {
                    e.onNext(i);
                }

                @Override
                public void onPageScrollStateChanged(int i) {

                }
            });
        });
    }
}
