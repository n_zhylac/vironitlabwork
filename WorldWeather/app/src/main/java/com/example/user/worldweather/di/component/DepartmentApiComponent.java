package com.example.user.worldweather.di.component;

import com.example.user.worldweather.di.module.DepartmentModule;
import com.example.user.worldweather.di.scopes.ApplicationScope;
import com.example.user.worldweather.retrofit.DepartmentsJsonApi;

import dagger.Component;

@ApplicationScope
@Component(modules = DepartmentModule.class)
public interface DepartmentApiComponent {
    DepartmentsJsonApi getDepartmentJsonApi();
}
