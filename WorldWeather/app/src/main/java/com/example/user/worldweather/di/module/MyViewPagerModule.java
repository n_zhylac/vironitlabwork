package com.example.user.worldweather.di.module;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.example.user.worldweather.Adapters.MyViewPagerAdapter;
import com.example.user.worldweather.di.scopes.ApplicationScope;
import com.example.user.worldweather.ui.MainActivity;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module(includes = ActivityModule.class)
public class MyViewPagerModule {

    @ApplicationScope
    @Provides
    public MyViewPagerAdapter pagerViewAdapter(FragmentManager fm){
        return MyViewPagerAdapter.getInstance(fm);
    }

    @ApplicationScope
    @Provides
    public FragmentManager getModuleFragmentManager(@Named("activity_context") Context context){
        FragmentManager manager = ((MainActivity) context).getSupportFragmentManager();
        return manager;
    }
}
