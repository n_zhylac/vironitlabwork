package com.example.user.worldweather.di.module;

import com.example.user.worldweather.di.scopes.ApplicationScope;
import com.example.user.worldweather.retrofit.DepartmentsJsonApi;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = FactoriesForRetrofitModule.class)
public class DepartmentModule {

    @ApplicationScope
    @Provides
    public DepartmentsJsonApi getDepartmentApi(Retrofit retrofit){
        return retrofit.create(DepartmentsJsonApi.class);
    }

    @ApplicationScope
    @Provides
    public Retrofit retrofit(GsonConverterFactory gsonConverterFactory, RxJava2CallAdapterFactory rxJava2CallAdapterFactory){
        return new Retrofit.Builder()
                .baseUrl("http://ais.brs.by:38516")
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .addConverterFactory(gsonConverterFactory)
                .build();
    }
}
