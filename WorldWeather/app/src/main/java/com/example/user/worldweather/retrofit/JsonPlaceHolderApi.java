package com.example.user.worldweather.retrofit;

import com.example.user.worldweather.data.Pojo.City;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface JsonPlaceHolderApi {
    @GET("/data/2.5/weather")
    Observable<City> getCityWithName(@Query("q") String name, @Query("appid") String key);
}
