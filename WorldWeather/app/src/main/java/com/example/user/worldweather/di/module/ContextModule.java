package com.example.user.worldweather.di.module;

import android.content.Context;

import com.example.user.worldweather.di.scopes.ApplicationScope;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class ContextModule {
    Context mContext;

    public ContextModule(Context context){
        this.mContext = context;
    }

    @Named("application_context")
    @ApplicationScope
    @Provides
    public Context context(){ return mContext.getApplicationContext(); }
}

