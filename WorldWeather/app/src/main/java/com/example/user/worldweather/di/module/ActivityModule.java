package com.example.user.worldweather.di.module;

import android.content.Context;

import com.example.user.worldweather.di.scopes.ApplicationScope;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {
    Context mContext;

    public ActivityModule(Context context){
        this.mContext = context;
    }

    @Named("activity_context")
    @ApplicationScope
    @Provides
    public Context context(){ return mContext; }
}
