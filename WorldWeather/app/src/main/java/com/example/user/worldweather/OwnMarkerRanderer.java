package com.example.user.worldweather;

import android.content.Context;

import com.example.user.worldweather.data.BelRosStrahMarker;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

public class OwnMarkerRanderer extends DefaultClusterRenderer<BelRosStrahMarker> {
    public OwnMarkerRanderer(Context context, GoogleMap map, ClusterManager clusterManager) {
        super(context, map, clusterManager);
    }

    protected void onBeforeClusterItemRendered(BelRosStrahMarker item, MarkerOptions markerOptions) {
        markerOptions.icon(item.getMarker().getIcon());
    }
}
