package com.example.user.worldweather.data;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterItem;

public class BelRosStrahMarker implements ClusterItem {

    private Bitmap mMyBitmap;
    private LatLng mLatLng;

    private MarkerOptions mMarker;

    public BelRosStrahMarker(int id, double latitude, double longitude, Bitmap bitmap) {
        mMyBitmap = bitmap;
        mLatLng = new LatLng(latitude, longitude);
        setMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .icon(BitmapDescriptorFactory.fromBitmap(makeMarker(id))));
    }

    @Override
    public LatLng getPosition() {
        return mLatLng;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getSnippet() {
        return null;
    }

    public MarkerOptions getMarker() {
        return mMarker;
    }

    public void setMarker(MarkerOptions marker) {
        mMarker = marker;
    }

    private Bitmap makeMarker(int id) {
        String idText = "" + id;

        int width = mMyBitmap.getWidth() / 3;
        int height = mMyBitmap.getHeight() / 3;
        Log.i("ImakeMarker", "w=" + width);

        Bitmap newBitmap = Bitmap.createScaledBitmap(mMyBitmap, width, height, false);

        width = width / 2;
        height = height / 2;
        Log.i("ImakeMarker", "w/2=" + width);

        Canvas canvas = new Canvas(newBitmap);
        Paint paint = new Paint();
        paint.setColor(Color.BLUE);
        paint.setTextSize(40);
        paint.setStyle(Paint.Style.FILL);

        Rect rect = new Rect();

        paint.getTextBounds(idText, 0, idText.length(), rect);
        float widthT = paint.measureText(idText);
        float heightT = rect.height();

        canvas.drawText(idText,
                width - widthT / 2,
                height + heightT / 2,
                paint);

        return newBitmap;
    }
}
