package com.example.user.worldweather.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.user.worldweather.data.Pojo.City;
import com.example.user.worldweather.R;
import com.example.user.worldweather.di.component.DaggerNetWorkServiceComponent;
import com.example.user.worldweather.di.component.NetWorkServiceComponent;
import com.example.user.worldweather.rx.RxGetBitmap;
import com.example.user.worldweather.rx.RxListenerSetter;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class WeatherFragment extends Fragment {

    private static final String TEXT_VIEW_VALUE = "TEXT_VIEW";
    private static final String IMAGE_VIEW_VALUE = "IMAGE_VIEW";

    @BindView(R.id.cityEditText)
    EditText mCityEditText;
    @BindView(R.id.informationTextView)
    TextView mInformationTextView;
    @BindView(R.id.findButton)
    Button mFindButton;
    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;
    @BindView(R.id.imageView)
    ImageView mWeatherImage;

    private Disposable mFindButtonDisposable;
    private Disposable mBitmapDisposable;
    private Disposable mCityEditTextDisposable;
    private Disposable mCityEditTextKeyListenerDisposable;

    public static WeatherFragment newInstance() {
        return new WeatherFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.pager_weather_fragment, viewGroup, false);
        ButterKnife.bind(this, view);

        NetWorkServiceComponent daggerNetWorkServiceComponent = DaggerNetWorkServiceComponent.create();

        mCityEditTextDisposable = RxListenerSetter.getViewClickerObservable(mCityEditText)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                    if (mCityEditText.isFocusable()) {
                        clearViewFocus(mCityEditText);
                    } else {
                        setViewFocus(mCityEditText);
                    }

                    keyBoardUp(mCityEditText);
                });

        mCityEditTextKeyListenerDisposable = RxListenerSetter
                .getKeyListenerObservable(mCityEditText)
                .subscribe(i -> {
                    clearViewFocus(mCityEditText);
                    keyBoardDown(mCityEditText);
                });

        mFindButtonDisposable = RxListenerSetter
                .getViewClickerObservable(mFindButton)
                .subscribeOn(Schedulers.io())
                .subscribe(s -> {
                    mFindButton.setClickable(false);
                    mProgressBar.setVisibility(View.VISIBLE);
                    keyBoardDown(mFindButton);
                    clearViewFocus(mCityEditText);
                    String name = getCityFromEditText();

                    mBitmapDisposable = daggerNetWorkServiceComponent
                            .getCityWeatherApi()
                            .getCityWithName(name, "229d8224fe71eeacf591ae1d650ea579")
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(city -> {
                                        setWeatherInfo(city);
                                        mProgressBar.setVisibility(View.INVISIBLE);
                                    },
                                    c -> {
                                        mProgressBar.setVisibility(View.INVISIBLE);
                                        mWeatherImage.setImageBitmap(null);
                                        mWeatherImage.setVisibility(View.INVISIBLE);
                                        mInformationTextView.setText(c.getMessage());
                                        mFindButton.setClickable(true);
                                    });
                });

        return view;
    }

    private String getCityFromEditText() {
        if (mCityEditText.getText() != null)
            return mCityEditText.getText().toString();
        else Log.i("getCity", "No city");
        return null;
    }

    @SuppressLint({"CheckResult", "SetTextI18n"})
    private void setWeatherInfo(City city) {
        mProgressBar.setVisibility(View.INVISIBLE);

        String temp;
        double tempMax = city.getMain().getTempMax() - 273.15, tempMin = city.getMain().getTempMin() - 273.15;
        if (tempMax == tempMin)
            temp = String.valueOf(tempMin);
        else
            temp = String.valueOf(tempMin) + " - " + String.valueOf(tempMax);

        RxGetBitmap.getWeatherImageObservable(city.getWeather().get(0).getIcon())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(b -> {
                            mInformationTextView.setText("City: " + city.getName() +
                                    "\nTemp: " + temp + " \u00b0C" +
                                    "\nWind's spied: " + city.getWind().getSpeed().toString() + " m/s");
                            mWeatherImage.setImageBitmap(b);
                            mWeatherImage.setVisibility(View.VISIBLE);
                            mFindButton.setClickable(true);
                        },
                        e -> {
                            mWeatherImage.setVisibility(View.INVISIBLE);
                            mWeatherImage.setImageBitmap(null);
                            mInformationTextView.setText("City: " + city.getName() +
                                    "\nTemp: " + temp + " \u00b0C" +
                                    "\nWind's spied: " + city.getWind().getSpeed().toString() + " m/s");
                            mFindButton.setClickable(true);
                        });

    }

    private void keyBoardDown(View view) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void keyBoardUp(View view) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(
                view.getApplicationWindowToken(), InputMethodManager.SHOW_IMPLICIT, 0);
    }

    private void setViewFocus(View view) {
        view.setFocusableInTouchMode(true);
        view.requestFocus();
    }

    private void clearViewFocus(View view) {
        view.setFocusable(false);
        view.clearFocus();
    }

    @Override
    public void onResume() {
        super.onResume();
        clearViewFocus(mCityEditText);
        keyBoardDown(mCityEditText);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        compositeDisposable.addAll(mFindButtonDisposable, mCityEditTextDisposable, mCityEditTextKeyListenerDisposable);
        compositeDisposable.dispose();
        if (mBitmapDisposable != null) mBitmapDisposable.dispose();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(TEXT_VIEW_VALUE, mInformationTextView.getText().toString());
        Bitmap bmp = ((BitmapDrawable) mWeatherImage.getDrawable()).getBitmap();
        outState.putParcelable(IMAGE_VIEW_VALUE, bmp);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            String text = savedInstanceState.getString(TEXT_VIEW_VALUE);
            Bitmap bmp = savedInstanceState.getParcelable(IMAGE_VIEW_VALUE);
            mWeatherImage.setImageBitmap(bmp);
            mWeatherImage.setVisibility(View.VISIBLE);
            mInformationTextView.setText(text);
        }
    }
}
