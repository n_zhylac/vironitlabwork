package com.example.user.worldweather.di.module;

import com.example.user.worldweather.di.scopes.ApplicationScope;
import com.example.user.worldweather.retrofit.JsonPlaceHolderApi;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = FactoriesForRetrofitModule.class)
public class CityWeatherModule {

    @ApplicationScope
    @Provides
    public JsonPlaceHolderApi cityWeatherApi(Retrofit retrofit){
        return retrofit.create(JsonPlaceHolderApi.class);
    }
    @ApplicationScope
    @Provides
    public Retrofit retrofit(GsonConverterFactory gsonConverterFactory, RxJava2CallAdapterFactory rxJava2CallAdapterFactory){
        return new Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org")
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .addConverterFactory(gsonConverterFactory)
                .build();
    }
}
