package com.example.user.worldweather.rx;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import io.reactivex.Observable;

public class RxGetBitmap {
    public static Observable<Bitmap> getWeatherImageObservable(String icon) {
        return Observable.create(a -> {
            try {
                URL url = new URL("http://openweathermap.org/img/w/" + icon + ".png");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                a.onNext(myBitmap);
            } catch (IOException e) {
                a.onError(e);
            }
        });
    }
}
