package com.example.user.worldweather.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;

import com.example.user.worldweather.OwnMarkerRanderer;
import com.example.user.worldweather.R;
import com.example.user.worldweather.data.BelRosStrahMarker;
import com.example.user.worldweather.data.Pojo.Department;
import com.example.user.worldweather.di.component.DaggerDepartmentApiComponent;
import com.example.user.worldweather.di.component.DepartmentApiComponent;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterManager;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MapFragment extends SupportMapFragment {

    private GoogleMap mMap;
    private Disposable mDepartmentDisposable;
    private Department mDepartment;
    private Bitmap mBitmap;
    private ClusterManager<BelRosStrahMarker> mClusterManager;

    public static MapFragment getInstance() {
        return new MapFragment();
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bg_marker);

        DepartmentApiComponent departmentApiComponent = DaggerDepartmentApiComponent.create();

        mDepartmentDisposable = departmentApiComponent
                .getDepartmentJsonApi()
                .getDepartment()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(department -> {
                    mDepartment = department;
                    updateUI();
                });

        getMapAsync(googleMap -> {
            Log.i("onmapready", "tuta");
            mMap = googleMap;
            LatLng latLng = new LatLng(53.9, 27.56667);

            if (mClusterManager == null) {
                mClusterManager = new ClusterManager<BelRosStrahMarker>(this.getContext(), mMap);
                mClusterManager.setRenderer(new OwnMarkerRanderer(getContext(), mMap, mClusterManager));

                mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
                    @Override
                    public void onCameraMove() {
                        mClusterManager.cluster();
                    }
                });
            }

            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 5.5f));
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDepartmentDisposable.dispose();
    }

    private void updateUI() {
        if (mMap == null || mDepartment == null) {
            return;
        }

        int departmentsAmount = mDepartment.getItems().size();
        List<Department.Item> list = mDepartment.getItems();

        for (int count = 0; count < departmentsAmount; count++) {
            LatLng latLng = new LatLng(list.get(count).getLatitude(), list.get(count).getLongitude());
            mClusterManager.addItem(new BelRosStrahMarker(list.get(count).getId(),
                    latLng.latitude, latLng.longitude, mBitmap));
            /*MarkerOptions marker = new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(makeMarker(list.get(count).getId())));
            marker.position(latLng);
            mMap.addMarker(marker);*/
        }
    }

   /* private Bitmap makeMarker(int id) {
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bg_marker);
        String idText = "" + id;

        int width = bitmap.getWidth() / 3;
        int height = bitmap.getHeight() / 3;
        Log.i("ImakeMarker", "w=" + width);

        Bitmap newBitmap = Bitmap.createScaledBitmap(bitmap, width, height, false);

        width = width / 2;
        height = height / 2;
        Log.i("ImakeMarker", "w/2=" + width);

        Canvas canvas = new Canvas(newBitmap);
        Paint paint = new Paint();
        paint.setColor(Color.BLUE);
        paint.setTextSize(40);
        paint.setStyle(Paint.Style.FILL);

        Rect rect = new Rect();

        paint.getTextBounds(idText, 0, idText.length(), rect);
        float widthT = paint.measureText(idText);
        float heightT = rect.height();

        canvas.drawText(idText,
                width - widthT / 2,
                height + heightT/2,
                paint);

        return newBitmap;
    }*/
}











