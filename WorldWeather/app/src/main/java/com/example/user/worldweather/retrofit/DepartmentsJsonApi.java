package com.example.user.worldweather.retrofit;

import com.example.user.worldweather.data.Pojo.Department;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface DepartmentsJsonApi {
    @GET("/ords/mobile_user/v1_3/GetOfficesInfo/")
    Observable<Department> getDepartment();
}
