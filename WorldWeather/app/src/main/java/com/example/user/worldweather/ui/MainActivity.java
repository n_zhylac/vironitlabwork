package com.example.user.worldweather.ui;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.example.user.worldweather.R;
import com.example.user.worldweather.di.component.DaggerMyViewPagerAdapterComponent;
import com.example.user.worldweather.di.component.MyViewPagerAdapterComponent;
import com.example.user.worldweather.di.module.ActivityModule;
import com.example.user.worldweather.rx.RxListenerSetter;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;
import io.reactivex.disposables.Disposable;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.viewPager) ViewPager mViewPager;

    private PagerAdapter mPagerAdapter;

    private Disposable mPageChangeListenerDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Fabric.with(this, new Crashlytics());
        ButterKnife.bind(this);

        MyViewPagerAdapterComponent adapterComponent = DaggerMyViewPagerAdapterComponent
                .builder()
                .activityModule(new ActivityModule(this))
                .build();

        mPagerAdapter = adapterComponent.getPagerViewAdapter();
        mViewPager.setAdapter(mPagerAdapter);

        mPageChangeListenerDisposable = RxListenerSetter
                .getPageChangeListener(mViewPager)
                .subscribe(i -> {
                    Log.i("Disposable", "onPageSelected, position = " + i);
                });
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        mPageChangeListenerDisposable.dispose();
    }
}
